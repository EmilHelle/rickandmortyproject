import React from "react";

//styles
const cardStyle = {
  margin: "100px",
  display: "flex",
  border: "solid",
  borderWidth: "0.5px",
  borderColor: "lightGrey",
  borderRadius: "20px",
  width: "80%",
  height: "80%"
};

const labelStyle = {
  borderRadius: "5px",
  fontFamily: "romana",
  color: "white"
};

const descriptionContentStyle = {
  margin: "5px",
  padding: "2px",
};

const descriptionElementStyle = {
  border: "solid",
  borderWidth: "0.4px",
  borderRadius: "10px",
  borderColor: "lightGrey",
  width: "100px",
  height: "100px"
};

class CardPage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    let {
      character: { name, status, species, gender, image, origin }
    } = this.props;
    return (
      <div style={cardStyle}>
        <h1>{name}</h1>
        <div id="descriptionContent" style={descriptionContentStyle}>
        <div style={descriptionElementStyle}>
          <label style={{ backgroundColor: "#b7eb8f", ...labelStyle }}>
            Status
          </label>
          <p style={{ fontFamily: "romana" }}>{status}</p>
        </div>
        <div style={descriptionElementStyle}>
          <label style={{ backgroundColor: "#69c0ff", ...labelStyle }}>
            Species
          </label>
          <p style={{ fontFamily: "romana" }}>{species}</p>
        </div>
        <div style={descriptionElementStyle}>
          <label style={{ backgroundColor: "#d3adf7", ...labelStyle }}>
            Gender
          </label>
          <p style={{ fontFamily: "romana" }}>{gender}</p>
        </div>
        <div style={descriptionElementStyle}>
          <label style={{ backgroundColor: "#adc6ff", ...labelStyle }}>
            Origin
          </label>
          <p style={{ fontFamily: "romana" }}>{origin.name.substring(0,6)}</p>
        </div>
      </div>
        <div id="mediaContent">
          <img
            src={image}
            style={{
              width: "400px",
              height: "400px",
              borderRadius: "20px",
              margin: "10px"
            }}
            alt="characterImage"
          ></img>
        </div>
      </div>
    );
  }
}

export default CardPage;
