import React from "react";
import LocationCard from "../components/LocationCard";
import Masonry from "react-masonry-component";

const masonryOptions = {
  transitionDuration: 1000
};

class LocationsPage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.getLocationsData();
  }

  getLocationsData = () => {
    fetch("https://rickandmortyapi.com/api/location/")
      .then(response => response.json())
      .then(data => {
        this.props.setLocations(data.results);
      })
      .catch(error => console.error(error));
  };

  render() {
    let { editLocation, locations} = this.props;

    return (
      <div style={{ marginTop: "100px" }}>
        <Masonry options={masonryOptions}>
          {locations.map(locations => (
            <LocationCard
              location={locations}
              editLocation={editLocation}
            />
          ))}
        </Masonry>
      </div>
    );
  }
}

export default LocationsPage;
