import React from "react";
import CharacterCard from "../components/CharacterCard";
import Masonry from "react-masonry-component";

const masonryOptions = {
  transitionDuration: 1000
};

class CardsPage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    fetch("https://rickandmortyapi.com/api/character/")
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.props.setCharacters(data.results);
      })
      .catch(error => console.error(error));
  };

  render() {
    let { editCharacter, characters } = this.props;

    return (
      <div style={{ marginTop: "100px" }}>
        <Masonry options={masonryOptions}>
          {characters.map(character => (
            <CharacterCard
              character={character}
              editCharacter={editCharacter}
            />
          ))}
        </Masonry>
      </div>
    );
  }
}

export default CardsPage;
