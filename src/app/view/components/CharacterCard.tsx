import React from "react";
import { Link } from "react-router-dom";
import { Card, Icon, Image, Button } from "semantic-ui-react";


interface CardProps {
  character: any;
  editCharacter: any;
}

const CharacterCard: React.FC<CardProps> = ({ character, editCharacter }) => {
  let { id, name, status, species, gender, image, origin } = character;
  return (
    <Card style={{width: '300px', height: '500px', margin: '10px'}}>
      <Image src={image} wrapped ui={false} />
      <Card.Content>
        <Card.Header>{name}</Card.Header>
        <Card.Meta>
          <span className="text">{status}</span>
        </Card.Meta>
        <Card.Description>
          {species}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a style={{margin: '2px'}}>
        <Icon name='marker' />
          {origin.name}
        </a>
        <a style={{margin: '2px'}}>
        <Icon name='info' />
          {gender}
        </a>
        
        <Link
          to={`/characters/${id}`}
          onClick={() => editCharacter(character)}
          style={{marginLeft: '60px'}}
        >
          <Button >View</Button>
        </Link>
      </Card.Content>
    </Card>
  );
};

export default CharacterCard;
