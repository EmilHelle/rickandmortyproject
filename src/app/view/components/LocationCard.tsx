import React from "react";
import { Link } from "react-router-dom";
import { Card, Icon, Button } from "semantic-ui-react";

interface CardProps {
  location: any;
  editLocation: any;
}

const LocationCard: React.FC<CardProps> = ({ location, editLocation }) => {
  let { id, name, type, dimension, residents } = location;
  return (
    <Card style={{ width: "200px", height: "300px", margin: "10px" }}>
      <Card.Content>
        <Card.Header>{name}</Card.Header>
        <Card.Meta>
          <span className="text">{dimension}</span>
        </Card.Meta>
        <Card.Description>{type}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a style={{ margin: "2px" }}>
          <Icon name='info' />
          Residents: {residents.length}
        </a>

        <Link
          to={`/locations/${id}`}
          onClick={() => editLocation(location)}
        >
          <Button>View</Button>
        </Link>
      </Card.Content>
    </Card>
  );
};

export default LocationCard;
