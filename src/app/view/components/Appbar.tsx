import React from "react";
import { Link } from "react-router-dom";
import { Input, Menu } from "semantic-ui-react";

const buttonStyle = {
  borderRadius: "20px",
  backgroundColor: "white",
  width: "80px",
  height: "40px",
  borderWidth: "0.4px"
};

const inputStyle = {
  height: "30px",
  minWidth: "200px",
  maxWidth: "500px",
  margin: "auto",
  marginLeft: "20px",
  borderRadius: "10px",
  borderWidth: "0.8px"
};

class Appbar extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeItem: "home"
    };
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    let { activeItem } = this.state;
    let { searchTerm, handleSearch } = this.props;
    return (
      <Menu
        secondary
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "110%",
          boxShadow: "1px 0px 4px #8c8c8c"
        }}
      >
        <img
          src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAB5CAMAAADRVtyNAAAAb1BMVEX///8RsMgArcYAq8UAqcP8//8Ascnu+fv1/P0Or8jn9vme2uXy+/yn3efZ8fXe8vbJ6vDA5+6U1uJ3zNuC0N6y4epDvNDS7vMntsxkxteN1OHC5+6u4OlTwNOj2+Yzuc5rydlbxNWF1eKZ1OGL1+NkuBXXAAAZGUlEQVR4nO09h5ajOrK2BBhwThhn9877/298qKSSSsmAQ7tnbtfZc3faNiCpcmQw+AUPDsPtp5fwC3HY8OFw9OlF/EIMlg162OzTq/iFCKT1cDjMyk8v4xcicGANfqpPr+IXIrBrpNuQ/doHPxRyId2K4tPL6ACj0WSSfnoR3w6VkG7s8OlltMK2YjzLruvdpxfyvbAR6Bmyn24dpHshhcVK2Xn+6cV8I4wK2PTt0+togdmVDREYv/2DvkAZ5pAlkCX/4RueGuxIHtr/dH7vC0fGV6HPx7Dzn21cl1986EBkN38tjLJhwUJfgO/zM43rfHs5jgbpblMwFz2C5avx9rBZfHqRL4KvZovX0Bdr2Pvku9fTAcaMccbEfwLYkVKOseTfQNBWBNiCEuEqNnr+7vW0w6TyZFoQR5tPL/QVMMmEQAiRWsl/pvOTX2Nc4+Dn9umVvgKEj1NkeeSbIVt++4ruQbpfXzqiZ1jU8+lXVp8+vebnAChtH/hiLp3Tn2Vdnxq10w07AkGcs2L4d1tzMgA6DXxTyYP4WQ551Rk5BpLjp1f9BFxAiAU8ujOgpxj+rKDjA+gZsj+fXvUTcBY7CLigY2kiFfX3L+ke1P8x/MwEl3Bfhc5Qyv+w6MGhu/Yx+Ll8etUPQy5cnFCE7Uudw09zf9Kw8VY0nqpwWIP4CZDf3wJ72BH3dMxIO4A/jH8G+TmABDYc7xbTzeZQhTD0MyNU9yGH6JQMUBe1h58x7rMYfmBxdyH1VRCrsf6rDAUWiPOdntY/y5+LwZlfy4my0QLBN2PGsp9lXzewIjwiRBrjlfGuL1zQlIO+MX5bZoz9Fdb2LGnIKpvHZFj5ncJhtAoFL+IwJtm49Wp//bLUyyFJalcEMvyF0Ko9o3L5ePwBD0Mmrq+4fpfnl/QI3r2WQ3Lr9fu1XlzQ+B81vtzNRZCsS4Bqlz4BxflmWjPeb3mvAFeGMyc8uiH7q95NPjfGe4mcWsuvqHG5cPGjiEwwFjvMu+4obcRh8zDeZ3UvgZ1r5jgWAhUQ7N35n8al6WMkzo0JUGSR35xcM0E9YAvBEt7VZ5Dm05B/e/W55+VxK8CT1kTFBlMPr4Q/bMh71EVtKe1EZNXWwQ/yj7o26WjzqJ/zb69n8KKM3FrxnG7v7dGRBj/B+HkELpS2YpLR8YPQQFD7Zh0ZAtH53QZC7nlxNgtPLfz0OLs2SDeVX6AmDpx3N+Es/MRCN04KT8V/j2pf144HLtkwKkXfBgvPi8ss/HxZmwuWJjwEIjjj63RhL/PuIbKTxT8xSTWhCCoyqUPVvjrnVSV+3m/BujB2+Qc3oMARDq97rtivZwtM+3VIWML3juo+0+3Jn2GVaVcLW+Hn2yMOXgirqKl82b0LP1kRcg/B2epRBElXH79slNEtSPyoC3koGxkChZ8IDUzmbwqtpEMPLPm2cfDzKgMbNIdnDJUQo+3h1c8pfqLGpeWiKjRueMtFDij8BM233T5jyXs4S+d2DIlRlvec1253zcct206hXMsT5tLU6mOFzMjRR71naxMYpNrcOfAAKPwE2G30lQjP9T0MhOqHHY2fk0y8r/vhJ73y5P6+hdPoKLqBkENyDX3ytEQAR0MIlgukVciFM5Z01vfSTQzUBi4y4Pk3dbPsGW6NhEo0q5YOejpmuBupo+4xX4XZvg7SYq4IvZcTSGw4Po78htgHJAg/39y6B3z3EfyAncOyN1U25Ypi2Rb/RRfhF/914x+BH/Bkl4yzbLWd/pnaogeM+oCtjj5j7JyDcDXLizEQ1aJdHZ7gU1zVmN4APfW7ogo7xfl8NDI70DaNn57sxsZzFQWYCblciMpoXlviec/Cli0+sFchCjn8mHtGDAT+mB4vpX52/NNSEnD9tqAcBt8qmm1kKlDiRRa7mlbCqqqMuIIL6ZFPZNrMtyHwHN0Y+l0whx81LEiK8cECRYyCW9FBqXre2VOorLaGlmcEP/ucftl88D+d4q67BF8arhSaymK/xP4ewCM7TIf2qpY2VBA7fGLlWL5vul11JQS9MsJ+qhnsjaX3iJRGbNFsPfsSXxrLqDDI6xRdhlhivrKsJqoalEpnXi0N5gKLrLuSGBED7ha8jJhvRKhOVoeM8SSo2Ce70zKcZSFO8Jr7O3sIjvtYUZEW3ZNBaoV5rS+bbRsR4pPotuI8u9HgsZCazA7cWdpGSSTfDEDG6sNANP/GrgFRQ0MgZPkHDvm2AL3N11mjMc+z3W6xOEl86CiyKm/KD2tlOyX7ZyPaSx7rTkKbDczmGTlN0IJaaDfnqEWVx8zpXvRFFYxXZqeQdbZtC1oXlKrHcs+6JXx67rptmgMKaaDU+t48UqreAH6msnROdHxxbOxCxlb3n9fyNwW7Pl1hMnGp14DKCSq3pjQekMCP8X0amVvWMc2g2Y4Z2gwVrhPEqtB+oJGFKMHOUey5RQi+frTxZ8xIqT38+I4T0JdHp8Xbgp7bcNhqDM7HbcYDtMSF/TCTOgVP3hQrCfwYv08sKr96xwywJ7vh+J3R2AWrrwrrxiZbR/UqNVJubVtHoNQQsF9o8I2yVywgsLYZPxEMNtFEOIIrkZBbYzpH3ppAB18jjES9FMnlM47PFfjZWPjBQJcTQVhZm1GOU6oNv6Lxq/VPdDgHowS+aLGMyLa9I1iKzscPQZ+0exQovLnu6pzGuhuhAJR91PhZjMa1VpKtbnRzEkXW4hyJW0f8Nr0zJf+muCyBnzOVbyZSaiV4l46HJJFHrImLUfmGVPFr31R/CD+URvzy15SEF2gGXMeSrCDg4qDQ0yjUJGH1RnKIFjRFPSQiuNUtBdl6X8CJ84kloQx+FBOqIkXwks2+gE6UA20VGB3d8kyZms41gTUahpyeohIUFoE03EP4sYKE3lZL+iU5KkztMUIl24wztJgWFNGGRemG2/OIUMN1XwhCRj/ihZnHIpcrQhFhNhKPE3tWapYGONb+NAjQl/qUz6VF3Sp4pzytENGQfGh3tyK1JBJ3tBpVJ/RAkfXJZ6fE/NQS43ntlgnDk1pbISbws4CHVZrPoIw1YquataNYUIJWMIlZLJwk/lbjJ7UdHPlLEPBLyiDEfFKsp9zFoM1CMH631GG3rvTpjGwyoUhYTK3qPWoLKH+P6hAajrK4d+YRovhJe6AVDFXfCxdhY1wKlK/FvHFjv+GmlC/HS7pr2AMSqT5WtB+soR0gQXAIDiQQyEQcRZMq2B9MIhBlfgc/y6pxTjSh2ErQJKzLTWY3EDNb4cGzSMszSfWxq6WVQo1grHXO5+wknyDlxPiqqSPPjLEE3kGsMkzTts5kKqw0phURNcLQOho1L80u/X1tGbBCkuoPkiVNwKrIMQaSgudPkjlR/JzAgDKBMNth0f1LU8dD9rJDkk6MjtdGWiNbbUxmnnhr3PE29CxxnslZHhfT7viWWNQSBX5Tz0BehEvXRKf450h3La4mQWKJS61XRlTCw6lplBTD0nzJ19ZxhqOKE3MUEe07knKJxh6s9PV1cMiqhe6Z1VBwx+CAuF1BjMiVR60KQLxRFDF2bnVMCWGKP4WZhjsShIHcJ881krYptdDSXC7ldSPETNuCEFOpWZ9aPabSvgZ06YJ/cnIoWr4wrbcRP+GMI0F2kKbmQ1ygAdJhMSyuoq6Or+yuBcZ45jqiENSm7pyOEbiRCwgnkvRyveqQUJCLAuklDDgRJVTFPyDSFMco2zPin6JdQqhIElHjt5jkj9jCztFG0jQRcLbjWzMLP/rj4qZNSLWkCHuYaGcwWXCCBRdO5M7ygBRCrMfvl56rovwio0WUMCn8ErcGceyqZU3HwAZgmwlyA+2yZVrNCMWN1qlS4hFZrn038/0cyyRRJxZA6DTJDyeN6GseRMxb2QHpNh0yRtuadj4DECDuSuGd6ayS7onnL5yH9yBoqiMb3/ADjDr5ombPGjRqJu1WfizP8SxsVbDDhZujRDogTjkXUpzH8laIH0oSUm5VTiqTWDCFoDistBEPJ26DlH1OxwA3ZdbpeL3P1G3L+W7nizBSj+ee1GgtNS6rPdE4DRnAesHBnJVOuKGppjRvIGYzZ80hmFhYpxLAi5xFDcHPtT7A5UKROpbPqIdGkmoaP0TiSoOCjZQ0Vog2wTgZQUFZDaRpTGhlMy+ISmIVEvtoOa6ttnceSiIQRWcr9K1MJhf87B+QHQa0gWVBXa6zhyoop+RXsAAmndBsS6eKYDUu74iFMGq6brKWN8KDkluK+T8TpX8skSs/q+zwFMVPbtQRk1pkhHhGQUI0ttYzx8QfoMfq6/Vsl1kR1NJSjsVeBm9ZFvLa93H8sFtYHumokCrDQ8UXO/2eXdKCYhoCg1RXUdL8OCNkoLR9LJSFwt7iL9vyqeiN5BNmxlVAI+zkhGxoThPZJzbtg3ErA0nwo+f95Jer4jt+C0qXe5NEVrMwde4w7yGJCltVIh2meYdOSgrwJpFjmgKelkZfF3J/8lgXij1iNSsYt7BceWsELp648d1AQqs/9UAEZC9kFkrPibrFLjrTg/F6rTX+ihPZKHglP944smsWEdR+kwy9+zXMQUqjSC4l7kqQAkpiBXVpY88F39QNqDYAuwtWHuuGFffvp4t76YdLCz8KtSRJKT5S6Xj0HrSJrUpC7ZQmqtzdZbyPzQg17tHgRDTQeHDcZ/oaVkUD+ndHjUVYQqkEyQ0mThcOq9mn0u7/QFxN/g/ML3vws8BIedXRgVjxqW9eD9yeGlQORPyKjzAyJGnNRMDkoxx1TcRneWNINM4hJvpHZits+cV1rpLdq2MKlOrRBQQ1hsIPfHcw4atw0s0KBrP2zAI9RZHItwmoWdAsM1ZVBD9olTrxDBpt0sFXY8FKnVORf5P6MrlyJ0VsBet3B0h4s70z19V01pCQs6kvZmx/r8TZLRS3Idz1Jo8cFkyKZ8Olq04Em7facBaJsqnNPg3CsB5VnEUEP3N9uU0yVjwNRaNZoNyARIlST9R7FdLcaRpyKuFB0zYexYFD5a+6q2afiWUh4B1uLSLl/iw4Xk7864HEpLtAPIRwqszdUWvbkFwPXiVJ3vBQldbKlRPEGsaPqb512J8aQ1o0mfCGpEWViGAw74Om1W4Df6SCFRKRokggfrQdT4+nykmSuPMkYBmtXW6EqEJmSJUlnhKWFhkQlL46Zpt58rOljlbNa7UECRtrLq/UrLDrBAJAQfxoO7pw6mwt/CDZmVC3pBydLAFWJ9IZ2EtSipawluZQEVtz4uM6SUgRRcBWtuo6wmA6uRsCuvjaqPC7jRR3CgG3yLDQWezuOC3LNb5aI7VPi6l6MlZP740eWakgTkmDx5UpaMMoJzBmED/EJHW0HUW6NmZMukf+vbNMAiJewdYAfLEZ3snCj2rUoDa/JXxIPYc+8naPkOjHkqp7A3ycziwhhwE38e8U/5gP5meYRs/A7JzcaklISp7zVYoHznkd52o56jA3NdriPw0RUjMDjgD4J+Q1EC3s8DRxXkzgR5dTqHMlDlE1n5EsD3wP8Uo20edEn6Dib7HapMDUtmjbFYGpbUIe4Ii94WLsizARTgQR/9Zhv2qtpSOvLofmEvnCMFBA2XJnqaJor4rEiiDVUp0a/F9p98vOlBkekt7U33Ki2ySWY4pBED+IaxqRJAJfVntB3AguRrI0G8E5NrG6WF82dZo5QPgHmLXcbmdLb/qb1UmlFOGZrGpo2/2A4QJECFCcJDNDvzG5KwuY5UJUFr05CDgbgl2RbpeV0Df/FkS8uRV05itC5Bo/StREIiqSouDHMqqEaTytTvGD2EH742Y6tW+QSSKmxzzUwHR1LpEiIh4gkviDGiEkFL3EwLBJAFkMwIwvyHYCL0JZkHEtWYrYCkzstyZvOLbnCp1C4ienKomABnc4Yqx2APiRObSZvE5rIDVuKRoEtpN7BeN1tw4dWiVqVFtgPqn5VtEhYHOcRAfKzvWOcO+GgCMjS+RorIs5C7YQ9gXYfDJtK89EGx2+gCMa3a9AzZcZieJJwPig+mwZ2o0uc2QmaC5VkLGepIaMhgSt0BCru77kb8f9VQyCbiux44TMUhSVXjabtbsb4Z8VS7NdnZJ3625dgAQYsrGgHHZs5K8UKHljStZS7BuK8OMRpCwtFK2Qr8Oggh+lrqrKEzv3pDv2YYpDxotz6YlpVbokUj8AdJrm8NJ5UhKtBqGHJtSSYycYoX2CGLOzafO723Z82CoBtoLtImZJzXJ4hRnpLJKR7MGeK8IYc34gGUzJSh4fEoIL+cKgbi0i1wJRCfBxfa5d6kRxKIwPbZRIF8CY2MCa8TlQGut3oqE+0PiLNeNkkwx3i4wu1DxaMAWtYjNeasM5mXUqwDGGjrVrH2GgDbGaRWRcUMEUn7RZ56k+Oll34tcw6gR12DeX/Gz5KPoCk61e2TJbu1kicmTaSWQV5Q3/lL+NdWjvNHr69cET2rffoLttsDy/UrNMd0E2m6T1VcYH3Z/WF/vpzrQgY06EQ9lCmmnh02gc5lqro7NqAqvkO0l8ExVlaPiNK6XHP/oEaGGUKW8rGOmgn9tOF0hgNEWVbROMAJcr3e1V9H0Nu1PO74LV5KgKGAQRUjqZSUEYsJpBjxEnwWiHcFD9xEjOOs9CjlK5GTZc2pAK3MoX91aRtA+Zhx/sSrRl5QUEfEMM9FCEtCE/2xB1pNDstzYO0u2ZZIeCb7m5B0ShBnnz5rO64B/rtzL0F2gbAXFPspjz5P7Ddgn1Ck5JFVJTo+VW3BFIPCAslPccMZDEdhypiD6cvfxbws+r8dgKboFNaf6EHC8KanlOPlWMayosWe9BgHROaoj1ZhZ+wNT18BOdm1ENHZVpmqnDA32X686DvkYZK0I+3k3HIEIwC8SFpNTtMtvjximDHOiuIT7gt2bYvSq9bAO8gwliBEWj5fiCDHTlm7xHYMRbqaKdBgy2nx/oO7tmISEJjlI8JzlOEresBHzHItD25oM1rm5K8QMV+H5xrzUoqz39FQBjIYQpyG5zEHQ54y4jX1jwRGTJmiXXdxgf6zNuuBdMqUscgNJ/sCS93kP+BbEZpQsVEx6OSTyQ149s2diXsdBrxRnXTCTaHPLaDX3NExZqF10z32pAg+Nt7zyQ/NPndTjSGy9aGioDAMP4ECVwju4vLkT0bB4a+EBy7BEGn6wOO1NGJcz/BXcDNGO/NhXX7+IBh2y8a2Ls1335FgCV2eyvu0EB4f4Efpw7lEbdsoeYZ0Ciyi1bwlJKYIdJtwBFHcI6evivG2lsg9xPn2n6K0p7fQBEPz4p82TFzHiPj88aotNOWtcifnbuLpjGPFSjpRbd11HrCEp+9pm3jRZSl3yZBRNO9GviygQyccAvEugMuoWhTeDoF7Xw7m/7OCcBBwUF3HsUEHYC9zAPUYT0G28ooDJXge9KO6AmxnJ75v3RJpIRfP+xAZId6DwIPZ0Gjgk7VN6jgJQyiQ5dD4DGT++ZQBAplub6ljkkRyJnnYbLxR7RWUbq9Nazw/rVW2F7zELrAXl//GAQ/4Eh2M1WFLVCQQRVYKsXKJ8GRqRsu8XEWNxU+Li3oLZBZZfe9FIXif1e7xva97cpFKS3SskuIeqsenwzmPa519uRsqNWkwrLdbLndMcFmw3fAatIIOweSJ/pqZeYCPVstwhqs/C5OZFkAHE7X2C7Ar8+RfzSaCzaf/gA/GH9Vf3ybsyuE9Se+4MjuJ99d41bhHcXdOlI5xcwhEBVxb1FwAF+ehrv2Pz4+FNFn5OtZ7AE6+l325kWqi6KP1bd3AukCfeeAbF/AkGlNlDFA0+YwYAfor/1K4BbrOIuYIozO0htXeb9zOmqMuvb43eIwx/X0O0AOE7hCZKrSZ2FHvAdimj3By2zOiX3dEnJM/N35ZSD2xN3iILAT++33clNPaMqKnr1DiM7r3mtvH6ReycBvFS0EWl/7AQg4J400yMA+Ol9lVTCLW8nuQdjkrozQ5deE2Qc9ZSVJ+h1fWr8uygBur5lvL/AzwOezFwo1vbRdFFIa+1vm9da9XspbRT02z66HtgM8PPgyH+Amr0rQSdeZfmIoi/5q1wyU2/2AJ2EAOdtdBc4kLJ/5qVgy+vT88kjIPDziJ4X00VfoRGPpLn0RSSYVmhfdz0zsOKeN+3fATOePMTY89fghwz8y17l3030vLSuL5GRdR7829+a2QUWjwWfFjTZ9jDk7KE63vtgmjA7C7gsPBDrL4ZFwtiwbeh2K9yM7nmdcMmx0Lq7zlZT5F+kAH8ErMbb7PbkPXS72/NvZ6EwV23r3W+KY6te9d7QnwHP7kb37zbcM1suypel8CEu3ct9vkmP+x8ScM9CvjKN94LQl/83fd27Q4W86uXs5udwQet/Fk5DbRrI+Xyz2eJ1DnikAvQeQHn4+95R9ndB/mXsanfi8SvgwvvH8r7Y+6pA/zKY6fc8dZkh/QBM+/OPyJr0L3n5J0EVdTFeb7bveWfo7AH8DOoepVb/MlxA8/Bq+74Xhg7Od+fEhWGZPFEY+e+AdHqeSvi3Q1rFhmDegeMzIex/BdSbxN/VL6Ph+UT5fxJkE9rrgm2/8FqALuF3JUt+4Wm4ssf6Fn/he2CR1a91Mv4fOcAEkVYs4P4AAAAASUVORK5CYII="
          style={{
            maxHeight: "40px",
            width: "150px",
            opacity: 0.8,
            borderRadius: "10px",
            margin: "10px"
          }}
          alt="bannerImage"
        ></img>
        <Menu.Menu position="left">
          <div style={{ margin: "auto" }}>
            <Link to="/">
              <Menu.Item
                name="Home"
                active={activeItem === "home"}
                onClick={this.handleItemClick}
              />
            </Link>
          </div>
          <div style={{ margin: "auto" }}>
            <Link to="/characters">
              <Menu.Item
                name="Characters"
                active={activeItem === "characters"}
                onClick={this.handleItemClick}
              />
            </Link>
          </div>
          <div style={{ margin: "auto" }}>
            <Link to="/locations">
              <Menu.Item
                name="Locations"
                active={activeItem === "locations"}
                onClick={this.handleItemClick}
              />
            </Link>
          </div>
        </Menu.Menu>
        <Menu.Menu position="left">
          <Menu.Item>
            <Input
              icon="search"
              placeholder="Search..."
              value={searchTerm}
              onChange={handleSearch}
            />
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

export default Appbar;
