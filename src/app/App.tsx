import React from "react";
import CardsPage from "./view/pages/CardsPage";
import CardPage from "./view/pages/CardPage";
import Appbar from "./view/components/Appbar";
import { Switch, Route } from "react-router-dom";
import LocationsPage from "./view/pages/LocationsPage";

class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      characters: [],
      currentCharacter: {},
      locations: [],
      currentLocation: {},
      searchTerm: ""
    };
  }

  editCharacter = (character: any) => {
    this.setState({ currentCharacter: character });
  };

  editLocation = (location: any) => {
    this.setState({ currentLocation: location });
  };

  changeSearchTerm = e => {
    this.setState({ searchTerm: e.target.value });
    console.log(this.state.searchTerm);
  };

  setCharacters = (characters: []) => {
    this.setState({ characters });
  };

  setLocations = (locations: []) => {
    this.setState({ locations });
  };

  handleSearch = e => {
    let { characters, searchTerm } = this.state;

    this.changeSearchTerm(e);
    var updatedList = characters;
    updatedList = updatedList.filter(function(character) {
      return (
        character.name.toLowerCase().search(searchTerm.toLowerCase()) !== -1
      );
    });
    this.setState({ characters: updatedList });
  };

  render() {
    let {
      characters,
      currentCharacter,
      locations,
      currentLocation,
      searchTerm
    } = this.state;
    return (
      <div>
        <Appbar searchTerm={searchTerm} handleSearch={this.handleSearch} />
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <div style={{ marginTop: "100px" }}>
                <img
                  src="https://media.giphy.com/media/3oEduS9rYqwVaUTYSQ/giphy.gif"
                  style={{
                    borderRadius: "40px",
                    position: "relative",
                    top: "40%",
                    left: "30%"
                  }}
                ></img>
              </div>
            )}
          />
          <Route
            exact
            path="/characters"
            render={() => (
              <CardsPage
                characters={characters}
                setCharacters={this.setCharacters}
                editCharacter={this.editCharacter}
              />
            )}
          />
          <Route
            path="/characters/:id"
            render={() => <CardPage character={currentCharacter} />}
          />
          <Route
            exact
            path="/locations"
            render={() => (
              <LocationsPage
                locations={locations}
                setLocations={this.setLocations}
                editLocation={this.editLocation}
              />
            )}
          />
        </Switch>
      </div>
    );
  }
}

export default App;
